# !Ballgame

!Ballgame is tiny [Breakout](https://en.wikipedia.org/wiki/Breakout_(video_game)) clone which runs in an icon on the [RISC OS](https://en.wikipedia.org/wiki/RISC_OS) icon bar.

Target platform: [BBC BASIC V](https://en.wikipedia.org/wiki/BBC_BASIC) on the [Acorn Archimedes series](https://en.wikipedia.org/wiki/Acorn_Archimedes) with RISC OS 2. 

This also runs on [contemporary RISC OS versions](https://www.riscosopen.org/content/) on the [Raspberry Pi](https://www.raspberrypi.org/) and [other ARM computers](https://www.riscosopen.org/content/downloads). 

You may want to turn off the ARM cache to somewhat reduce the speed as the game was written for a 1987 32-bit ARM2 at 8 MHz without internal cache, and I never expected that this would ever run on 64-bit processors at GHz speeds with plenty of cache on board. 

To turn off the cache, press F12, and type:

    * cache off

The player's bat is controlled by the vertical mouse position. If you lose a ball, the score is reset to zero and a new game starts. This program is multi-tasking: you can open as many games on the icon bar as you have space for and try to keep many balls in the air while at the same time going on with whatever work you were doing in any window. (RISC OS uses co-operative rather than pre-emptive multitasking, meaning that programs have to include specific support for task switching.)

The source code is in the !Ballgame directory. The !RunImage file is the main BBC BASIC V program, it was created in Acorn's TWIN editor.

I also uploaded the Spark archive (ballgame.spk), which can be opened in RISC OS using [David Pilling's Spark utilities](https://www.davidpilling.com/spark.html).

Programmed on 13 and 14 May 1990. Uploaded on 27 March 2022.
